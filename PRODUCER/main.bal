import ballerinax/kafka;

public type Order record {|
    int orderId =12;
    string productName="milk";
    decimal productPrice=20;
    boolean isValid=true;
    string addess="Khomasdal";

|};
public type Customer record {|
    int customerId;
    string cutomerName;
    string customerlastName;
    string address;
    boolean isValid;
    
|};

public type Store1Products record {|
   int  Product_ID=12;
   int Product_Quantily=100;
    decimal Product_Price=20.00;
    string Product_Name="Milk";
    

|};
public type Store2Products record {|
   int  Product_ID=14;
   int Product_Quantily=50;
    decimal Product_Price=20.0;
    string Product_Name="Milk";
    

|};
public type Store3Products record {|
   int  Product_ID=15;
   int Product_Quantily=20;
    decimal Product_Price=20.00;
    string Product_Name="Milk";
    

|};
// Create a subtype of `kafka:AnydataProducerRecord`.
public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
    int key;
|};

kafka:Producer orderProducer = check new (kafka:DEFAULT_URL );

public function main() returns error? {
    OrderProducerRecord producerRecord = {
        key: 1,
        topic: "test-kafka-topic-dsa1",
        value: {
            orderId: 1,
            productName: "ABC",
            productPrice: 27.5,
            isValid: true
        }
    };
    // Sends the message to the Kafka topic.
    check orderProducer->send(producerRecord);
}